;(function(BV, $) {
$(window).scroll(function () { 
			if ($(window).scrollTop() > 150) {
				$(".topnav").addClass("active");
			}  else{
				$(".topnav").removeClass("active");
			}
		});
	$(function() {
		// Any globals go here in CAPS (but avoid if possible)

		// follow a singleton pattern
		// (http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript)

		BV.Config.init();

		$("#toggle-menu").click(function(e) {
			e.preventDefault();
			e.stopPropagation();
			$(".svg-menu-toggle").toggleClass("svg-menu-toggle-active");
			$(".menu").toggleClass("in");
		});
		$(document).click(function() {
			$(".menu").removeClass('in');
			$(".svg-menu-toggle").removeClass("svg-menu-toggle-active");
		});                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

	});// END DOC READY


	BV.Config = {
		variableX : '', // please don't keep me - only for example syntax!

		init : function () {
			console.debug('BV-light is running');

			var expanded = false;
			$('.expand-btn').html("More");
			$('.expand-btn').click(function(event){
				console.log("clicked");
			event.preventDefault();

			if (expanded == false) {
				$('.expand-btn').html("Less");
				expanded = true;
				$('.hidden').addClass('expanded').removeClass('hidden');
			} else {
				$('.expand-btn').html("More");
				expanded = false;
				$('.expanded').addClass('hidden').removeClass('expanded');
			}
		});
		}
	};

	// Example module
	/*
	BV.MyExampleModule = {
		init : function () {
			BV.MyExampleModule.setupEvents();
		},

		setupEvents : function () {
			//do some more stuff in here
		}
	};
	*/

})(window.BV = window.BV || {}, jQuery);